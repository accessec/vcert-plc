from vcert import CertificateRequest, Connection, CloudConnection, FakeConnection, TPPConnection, RevocationRequest
from src.certificateConverter import pemToDer
import os
from src.config_xm22 import config
import time
import logging

##> Fetches a certificate from Venafi, converts it to .der format. Uses config and mostly follows venafi vcert example!
def fullFetchVCert(config, certName="cert"):
		
	##> Set up variables!
	t = os.environ.get('TOKEN')
	c = Connection(url=config["VENUrl"], token=t, user=config["VENUser"], password=config["VENPassword"])
		
	##> Test connection!
	status = c.ping()
	if not status:
		logging.info("Unable to reach Venafi server!")
		exit(1)
	logging.info("Venafi server online!")
		
	##> If connection established, fetch certificate and wait for signing!
	request = CertificateRequest(common_name="localhost", origin=config["CertificateRequestOrigin"])
	zone_config = c.read_zone_conf(config["VENZone"])
	request.update_from_zone_config(zone_config)
	c.request_cert(request, config["VENZone"])
	logging.info("Got certificate!")

	while True: ##> Not the most elegant way to do this!
		cert = c.retrieve_cert(request)
		if cert:
			break
		else:
			time.sleep(2)
	
	##> cert.full_chain -> Certificate in .pem format, request.private_key_pem -> Private key!
	logging.info("Writing out...")
	f = open(config["localCertFolder"] + "/" + certName + ".pem", "w")
	f.write(cert.full_chain)
	f.close()
	
	##> Convert .pem to .der!	
	pemToDer(config["localCertFolder"] + "/" + certName)
	
	##> Delete .pem file!
	os.remove(config["localCertFolder"] + "/" + certName + ".pem")
	
	##> return full cert path!
	logging.info("Done!")
	return str(config["localCertFolder"] + "/" + certName + ".der")